function sweet(capt, isi, ikon, tombol) {
  swal({
    title: capt,
    text: isi,
    icon: ikon,
    button: tombol,
  });
}