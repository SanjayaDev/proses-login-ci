<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_data extends CI_Model {
  protected $data;
  protected $table;
  protected $where;

  public function insertData($data,$table) 
  {
    $this->db->insert($table,$data);
  }

  public function editData($where,$table)
  {
    return $this->db->get_where($table,$where);
  }

  public function getData($table)
  {
    return $this->db->get($table);
  }

  public function updateData($data,$where,$table)
  {
    $this->db->where($where)->update($table,$data);
  }

  public function deleteData($where,$table)
  {
    $this->db->delete($table,$where);
  }
}