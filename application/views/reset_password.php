<?= $this->session->flashdata('pesan'); ?>
<div class="container">
  <div class="row mt-5">
    <div class="col-md-6 mx-auto">
      <div class="card">
        <div class="card-body">
          <?= form_open('resetPassword'); ?>
            <div class="form-group">
              <label>Masukan Password Baru</label>
              <input type="password" name="pass" class="form-control" required>
              <?= form_error('pass','<small class="text-danger">','</small>'); ?>
            </div>
            <div class="form-group">
              <label>Tulis Ulang Password Baru</label>
              <input type="password" name="pass1" class="form-control" required>
              <?= form_error('pass1','<small class="text-danger">','</small>'); ?>
            </div>
            <input type="submit" value="Verifikasi" class="btn btn-success btn-sm">
          <?= form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>