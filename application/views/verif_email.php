<div class="container">
  <div class="row mt-5">
    <div class="col-md-6 mx-auto">
      <div class="card">
        <div class="card-body">
          <?= form_open('lupaPassword'); ?>
            <div class="form-group">
              <label>Masukan alamat email akun kamu</label>
              <input type="email" name="email" class="form-control" required>
              <?= form_error('email','<small class="text-danger">','</small>'); ?>
            </div>
            <input type="submit" value="Verifikasi" class="btn btn-success btn-sm">
          <?= form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>